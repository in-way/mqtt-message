import request from '@/utils/request'

// 查询菜单列表
export function listMenu(query) {
  return request({
    url: '/soa-system/menu/list',
    method: 'get',
    params: query
  })
}

// 根据数据权限查询菜单列表
export function listMenuByPower(query) {
  return request({
    url: '/soa-system/menu/listByPower',
    method: 'get',
    params: query
  })
}

// 查询菜单详细
export function getMenu(menuId) {
  return request({
    url: '/soa-system/menu/' + menuId,
    method: 'get'
  })
}

// 查询菜单下拉树结构
export function treeselect(query) {
  return request({
    url: '/soa-system/menu/treeselect',
    method: 'get',
    params: query
  })
}


// 角色管理查询菜单下拉树结构
export function roleTreeselect(query) {
  return request({
    url: '/soa-system/menu/roleTreeSelect',
    method: 'get',
    params: query
  })
}

// 根据角色ID查询菜单下拉树结构
export function roleMenuTreeselect(roleId,appId) {
  return request({
    url: `/soa-system/menu/roleMenuTreeselect/${roleId}/${appId}`  ,
    method: 'get'
  })
}

// 新增菜单
export function addMenu(data) {
  return request({
    url: '/soa-system/menu',
    method: 'post',
    data: data
  })
}

// 修改菜单
export function updateMenu(data) {
  return request({
    url: '/soa-system/menu',
    method: 'put',
    data: data
  })
}

// 删除菜单
export function delMenu(menuId) {
  return request({
    url: '/soa-system/menu/' + menuId,
    method: 'delete'
  })
}