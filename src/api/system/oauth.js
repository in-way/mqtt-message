import request from '@/utils/request'

// 查询应用列表
export function getCode(query) {
    return request({
        url: '/soa-system/oauth/selfCode',
        method: 'get',
        params: query
    })
}
// 认证应用的退出
export function appLogout(query) {
    return request({
        url: '/soa-system/oauth/logout',
        method: 'get',
        params: query
    })
}