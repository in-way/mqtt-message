import request from '@/utils/request'

// 查询权限列表
export function listPower(query) {
  return request({
    url: '/soa-system/power/list',
    method: 'get',
    params: query
  })
}
// 查询所有权限列表
export function listPowerAll(query) {
  return request({
    url: '/soa-system/power/listAll',
    method: 'get',
    params: query
  })
}

// 查询菜单下拉树结构
export function treeSelect(query) {
  return request({
    url: '/soa-system/power/treeSelect',
    method: 'get',
    params: query
  })
}

// 根据角色ID查询菜单下拉树结构
export function rolePowerTreeSelect(roleId) {
  return request({
    url: '/soa-system/power/rolePowerTreeSelect/' + roleId,
    method: 'get'
  })
}


// 查询权限详细
export function getPower(powerId) {
  return request({
    url: '/soa-system/power/' + powerId,
    method: 'get'
  })
}

// 新增权限
export function addPower(data) {
  return request({
    url: '/soa-system/power',
    method: 'post',
    data: data
  })
}

// 修改权限
export function updatePower(data) {
  return request({
    url: '/soa-system/power',
    method: 'put',
    data: data
  })
}

// 删除权限
export function delPower(powerId) {
  return request({
    url: '/soa-system/power/' + powerId,
    method: 'delete'
  })
}
