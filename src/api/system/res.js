import request from '@/utils/request'

// 查询门户素材（资源）列表
export function listRes(query) {
  return request({
    url: '/protal/res/list',
    method: 'get',
    params: query
  })
}

// 查询门户素材（资源）详细
export function getRes(resId) {
  return request({
    url: '/protal/res/' + resId,
    method: 'get'
  })
}

// 根据id查询门户素材（资源）详细
export function getResList(data) {
  return request({
    url: '/protal/res/getInfoList',
    method: 'post',
    data: data
  })
}

// 新增门户素材（资源）
export function addRes(data) {
  return request({
    url: '/protal/res',
    method: 'post',
    data: data
  })
}

// 修改门户素材（资源）
export function updateRes(data) {
  return request({
    url: '/protal/res',
    method: 'put',
    data: data
  })
}

// 删除门户素材（资源）
export function delRes(resId) {
  return request({
    url: '/protal/res/' + resId,
    method: 'delete'
  })
}


// 查询门户素材分类（文件夹）详细
export function getType(typeId) {
  return request({
    url: '/protal/type/' + typeId,
    method: 'get'
  })
}

// 新增门户素材分类（文件夹）
export function addType(data) {
  return request({
    url: '/protal/type',
    method: 'post',
    data: data
  })
}

// 修改门户素材分类（文件夹）
export function updateType(data) {
  return request({
    url: '/protal/type',
    method: 'put',
    data: data
  })
}

// 删除门户素材分类（文件夹）
export function delType(typeId) {
  return request({
    url: '/protal/type/' + typeId,
    method: 'delete'
  })
}
