import request from '@/utils/request'

// 查询门户素材分类（文件夹）列表
export function listType(query) {
  return request({
    url: '/protal/type/list',
    method: 'get',
    params: query
  })
}

// 查询门户素材分类（文件夹）详细
export function getType(typeId) {
  return request({
    url: '/protal/type/' + typeId,
    method: 'get'
  })
}

// 新增门户素材分类（文件夹）
export function addType(data) {
  return request({
    url: '/protal/type',
    method: 'post',
    data: data
  })
}

// 修改门户素材分类（文件夹）
export function updateType(data) {
  return request({
    url: '/protal/type',
    method: 'put',
    data: data
  })
}

// 删除门户素材分类（文件夹）
export function delType(typeId) {
  return request({
    url: '/protal/type/' + typeId,
    method: 'delete'
  })
}
// 查询门户素材分类（文件夹）列表
export function listTypeAll(query) {
  return request({
    url: '/protal/type/listAll',
    method: 'get',
    params: query
  })
}