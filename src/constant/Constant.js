//成功状态
export const SUCCESS =200;
//未知请求
export const NOT_FOUNT =404;
//系统错误
export const SYSTEM_ERROR =500;
//无访问权限
export const NOT_AUTH =401;
//三方应用退出KEY
export const APP_LOGOUT_KEY ='logoutAppUrls';
export const MIDDLEWARE_TYPE ={
    '时序数据库':1,
    'MQ消息服务':2,
    'MQTT消息服务':3,
}
export const MIDDLEWARE_STATUS ={
    'online':'在线',
    'offline':'离线',
    'abnormal':'异常',
    'highload':'高负荷',
    'unbound':'未绑定',
    'notenabled':'未启用',
}
export const MIDDLEWARE_STATUS_STYLE ={
    'online':'success',
    'offline':'info',
    'abnormal':'danger',
    'highload':'danger',
    'unbound':'info',
    'notenabled':'info',
}

//TOPIC前缀
export const TOP_PREFIX ='collect/'
//边状态 200 状态正常 402 license未授权 701 未初始化
export const EDGE_STATUS ="edge_status";