/**
 * mq配置
 */
export interface IMQConfig {
  id: string, // 客户端id
  mqtt: string, // mqtt地址
  username: string, // 用户名
  password: string, // 密码
  connectTimeout: number, // 超时
}
