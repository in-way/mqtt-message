import {defineStore} from "pinia/dist/pinia";

const usePageCtx = defineStore(
    'pageCtx',
    {
        state: () => ({
            ctx: null
        }),
    })

export function swapPageCtx(newCtx, callback = undefined) {
    let ctx = window.history.state?.ctx;
    let state = window.history.state
    if (state) {
        state.ctx = JSON.stringify(newCtx)
    }
    window.history.replaceState(state, "")
    // let ctx = usePageCtx().ctx;
    // usePageCtx().ctx = JSON.stringify(newCtx)
    try {
        if (callback && ctx) {
            callback(JSON.parse(ctx))
        }
    } catch (e) {
    }
}
