/*本地窗口外链跳转url*/
import {defineStore} from "pinia/dist/pinia";

const iframeUrl = defineStore('url',{
      state:()=>({
        url:''
      }),
      actions:{
          getUrl(){
              return this.url;
          },
          setUrl(_url){
           this.url = _url
          }
      }
    }
)
export default iframeUrl
