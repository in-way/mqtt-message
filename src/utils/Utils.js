function transHref() {
    let localtionUrl = window.location.href;
    if(localtionUrl.indexOf("?")!=-1){
        localtionUrl = localtionUrl.substring(localtionUrl.indexOf("?") + 1);
    }else{
        return "";
    }

    if(localtionUrl.indexOf("#")!=-1){
        localtionUrl = localtionUrl.substring(0,localtionUrl.indexOf("#"));
    }
    return localtionUrl;
}
//内部私有函数
class UrlUtils{

    //URL参数获取
    static getQueryString(name) {
       let localtionUrl = transHref();
       let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
       let r = localtionUrl.match(reg);
       if (r !== null) return unescape(r[2]);
       return null;
   };
    static getAllQueryParams()
   {
       let localtionUrl = transHref();
       let strs = localtionUrl.split("&");
       let theRequest = {};
       for(var i = 0; i < strs.length; i ++) {
           theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
       }
       return theRequest;
   }
   static getParamsStr(params)
   {
       let sourceParams = UrlUtils.getAllQueryParams();
       if(!params)
       {
           params = {};
       }
       Object.assign(sourceParams,params);

       let pars = [];
       for(var key in sourceParams){
           if (sourceParams.hasOwnProperty(key)) {
               pars.push(key+"="+sourceParams[key]);
           };
       }
       return pars.join("&");
   }
   static getWebUrl(url)
   {
       if(url.indexOf("#")==-1)
       {
           return url;
       }
       let index = url.indexOf("#");
       let begin = url.substring(0,index);
       let end = url.substring(index);
       let hash = end;
       let param = "";
       if(end.indexOf("?"))
       {
           hash = end.substring(0,end.indexOf("?"));
           param = end.substring(end.indexOf("?"));
       }
       return begin+param+hash;
   }

   static getUrl(url,params,excude,skipBackSchemaKey)
   {
       let sourceParams = UrlUtils.getAllQueryParams();
       if(!params)
       {
           params = {};
       }
       if(!skipBackSchemaKey && sourceParams[UrlUtils._back_schema_key_])
       {
           sourceParams["g_schema_key"] = sourceParams[UrlUtils._back_schema_key_];
       }

       Object.assign(sourceParams,params);

       let pars = [];
       for(var key in sourceParams){
           if (sourceParams.hasOwnProperty(key)) {
               //filter,只输出sourceParams的私有属性
               if(excude && $.inArray(key,excude)>=0)
               {
                   continue;
               }
               pars.push(key+"="+sourceParams[key]);
           };
       }


       let htmlIndex = location.href.indexOf(".html");
       return location.href.substring(0,htmlIndex)+".html"+url+"?"+pars.join("&");
   }

    static getContextPath(){
       let url = location.href;
       let begin = 0;
       if(url.startsWith("http://"))
       {
           begin = 7;
       }else if(url.startsWith("https://"))
       {
           begin = 8;
       }
       begin = url.indexOf("/",begin);
       let end = url.indexOf("query.html");
       if(end<=0)
       {
           end = url.indexOf("mQuery.html");
       }
       let content = url.substring(begin+1,end-1);
       let first = content.indexOf("/");
        if(first==-1)
        {
            return content == ""?"":"/"+content;
        }else {
            return "/"+content.substring(0,first)=="/"?"":"/"+content.substring(0,first);
        }
   }
   static transIframeUrl(url)
   {
       if(!url)
       {
           return;
       }
       let gId = UrlUtils.getQueryString("g_id");
       if(!gId)
       {
           gId = "0";
       }
       url = url.replace("{{g_id}}",gId?gId:"");
       url = url.replace("{{context_path}}",UrlUtils.getContextPath());
       //将hash放后面
       url = UrlUtils.getWebUrl(url);
       return url;
   }

   static addParams(url,params)
   {
       if(url.indexOf("?")==-1)
       {
           return url+"?"+params;
       }
       let index = url.indexOf("?");
       if(params.substring(0,1) == "&")
       {
           params = params.substring(1);
       }
       if(params.substring(params.length-1) != "&")
       {
           params = params+"&";
       }
       return url.substring(0,index+1)+params+url.substring(index+1)
   }
};

class DateUtils{
    /** * 对Date的扩展，将 Date 转化为指定格式的String * 月(M)、日(d)、12小时(h)、24小时(H)、分(m)、秒(s)、周(E)、季度(q)
     可以用 1-2 个占位符 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) * eg: * (new
     Date()).pattern("yyyy-MM-dd hh:mm:ss.S")==> 2006-07-02 08:09:04.423
     * (new Date()).pattern("yyyy-MM-dd E HH:mm:ss") ==> 2009-03-10 二 20:09:04
     * (new Date()).pattern("yyyy-MM-dd EE hh:mm:ss") ==> 2009-03-10 周二 08:09:04
     * (new Date()).pattern("yyyy-MM-dd EEE hh:mm:ss") ==> 2009-03-10 星期二 08:09:04
     * (new Date()).pattern("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
     */
    static formate(d,fmt)
    {
        var o = {
            "M+" : d.getMonth()+1, //月份
            "d+" : d.getDate(), //日
            "h+" : d.getHours()%12 == 0 ? 12 : d.getHours()%12, //小时
            "H+" : d.getHours(), //小时
            "m+" : d.getMinutes(), //分
            "s+" : d.getSeconds(), //秒
            "q+" : Math.floor((d.getMonth()+3)/3), //季度
            "S" : d.getMilliseconds() //毫秒
        };
        var week = {
            "0" : "/u65e5",
            "1" : "/u4e00",
            "2" : "/u4e8c",
            "3" : "/u4e09",
            "4" : "/u56db",
            "5" : "/u4e94",
            "6" : "/u516d"
        };
        if(/(y+)/.test(fmt)){
            fmt=fmt.replace(RegExp.$1, (d.getFullYear()+"").substr(4 - RegExp.$1.length));
        }
        if(/(E+)/.test(fmt)){
            fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[d.getDay()+""]);
        }
        for(var k in o){
            if(new RegExp("("+ k +")").test(fmt)){
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
            }
        }
        return fmt;
    }
    static toDb(s)
    {
        var s= s.replace(/[^0-9]/ig,"");
        return s;
    }
}

export {UrlUtils};
export {DateUtils};