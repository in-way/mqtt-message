// @ts-ignore
import postRobot from "post-robot";
import { ACTIONS, ActionType } from "../actions";

export function dispatch(
  actionType: ActionType,
  data: Record<string, unknown>
) {
  if (window.top === window.self) {
      postRobot
        .send(window.self, ACTIONS[actionType], data, {
          domain: window.location.origin,
        })
        .catch((err: unknown) => {
          console.error("send message to parent faild", err);
        });
  };
  if (!ACTIONS[actionType]) throw new Error("action is not defined");
  // postRobot
  //   .send(window.parent, ACTIONS[actionType], data, {
  //     domain: window.location.origin,
  //   })
  //   .catch((err: unknown) => {
  //     console.error("send message to parent faild", err);
  //   });
}


export function dispatchTop(
    actionType: ActionType,
    data: Record<string, unknown>
) {
  if (window.top === window.self) {
      postRobot
          .send(window.self, ACTIONS[actionType], data, {
              domain: window.location.origin,
          })
          .catch((err: unknown) => {
              console.error("send message to parent faild", err);
          });
  };
  if (!ACTIONS[actionType]) throw new Error("action is not defined");
  postRobot
      .send(window.top, ACTIONS[actionType], data, {
        domain: window.location.origin,
      })
      .catch((err: unknown) => {
        console.error("send message to parent faild", err);
      });
}