import useUserStore from '@/store/modules/user'

function authPermission(permission) {
  const all_permission = "*:*:*";
  const permissions = useUserStore().permissions
  if (permission && permission.length > 0) {
    return permissions.some(v => {
      return all_permission === v || v === permission
    })
  } else {
    return false
  }
}

/**
 * 登录用户的其他权限
 * @param permission
 * @returns {boolean}
 */
function authOtherPermission(permission) {
  const all_other_permission = "*:*:*";
  const permissions = useUserStore().otherPermissions
  if (permission && permission.length > 0) {
    return permissions.some(v => {
      return all_other_permission === v || v === permission
    })
  } else {
    return false
  }
}
function authRole(role) {
  const super_admin = "admin";
  const roles = useUserStore().roles
  if (role && role.length > 0) {
    return roles.some(v => {
      return super_admin === v || v === role
    })
  } else {
    return false
  }
}

export default {
  // 验证用户是否具备某权限
  hasPermi(permission) {
    return authPermission(permission);
  },
  // 验证用户是否含有指定权限，只需包含其中一个
  hasPermiOr(permissions) {
    return permissions.some(item => {
      return authPermission(item)
    })
  },
  // 验证用户是否含有指定权限，必须全部拥有
  hasPermiAnd(permissions) {
    return permissions.every(item => {
      return authPermission(item)
    })
  },
  // 验证用户是否具备某其他权限
  hasOtherPermi(permission) {
    return authOtherPermission(permission);
  },
  // 验证用户是否含有指定其他权限，只需包含其中一个
  hasOtherPermiOr(permissions) {
    return permissions.some(item => {
      return authOtherPermission(item)
    })
  },
  // 验证用户是否含有指定其他权限，必须全部拥有
  hasOtherPermiAnd(permissions) {
    return permissions.every(item => {
      return authOtherPermission(item)
    })
  },
  // 验证用户是否具备某角色
  hasRole(role) {
    return authRole(role);
  },
  // 验证用户是否含有指定角色，只需包含其中一个
  hasRoleOr(roles) {
    return roles.some(item => {
      return authRole(item)
    })
  },
  // 验证用户是否含有指定角色，必须全部拥有
  hasRoleAnd(roles) {
    return roles.every(item => {
      return authRole(item)
    })
  },
  //是管理员
  isAdmin(){
    return useUserStore().isAdmin
  },
  //是单位管理员
  isOrgAdmin(){
    return useUserStore().isOrgAdmin
  },
  //是单位管理员
  getOrgId(){
    return useUserStore().orgId
  },
  getRoles(){
    return useUserStore().roles;
  }
}
