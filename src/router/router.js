import { createRouter,createWebHashHistory } from 'vue-router'
import { TUICore } from '../TUIKit';
const router = createRouter({
  history: createWebHashHistory(), // 路由模式
//   routes:[
// {
//         path: '/',
//         name: '',
//         redirect:'/index',
//         component: () => import('@/pages/layout.vue'),
//         children:[
//           {
//                 path: '/index',
//                 name: 'index',
//                 component: () => import('@/pages/index.vue'),
//           },
//           {
//             path: '/productmall',
//             name: 'productmall',
//             component: () => import('@/pages/product/productmall.vue'),
//           },
//           // {
//           //   path: '/my',
//           //   name: 'my',
//           //   redirect:'/my/myindex',
//           //   component: () => import('../pages/my/my.vue'), // 路由懒加载
//           //   children: [
//           //     {
//           //       path: 'myindex',
//           //       name: 'myindex',
//           //       component: () => import('../pages/my/myindex.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'order',
//           //       name: 'order',
//           //       component: () => import('../pages/order/order.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'mymessage',
//           //       name: 'mymessage',
//           //       component: () => import('../pages/my/mymessage.vue') ,// 路由懒加载
//           //
//           //     },
//           //     {
//           //       path: 'enterprise',
//           //       name: 'enterprise',
//           //       component: () => import('../pages/my/enterprise.vue') ,// 路由懒加载
//           //       // children: [
//           //       //   {
//           //       //     path: 'companyregister',
//           //       //     name: 'companyregister',
//           //       //     component: () => import('../pages/my/companyregister.vue') // 路由懒加载
//           //       //   }
//           //       // ]
//           //     },
//           //     {
//           //       path: 'userinfo',
//           //       name: 'userinfo',
//           //       component: () => import('../pages/my/userinfo.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'address',
//           //       name: 'address',
//           //       component: () => import('../pages/my/address.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'check',
//           //       name: 'check',
//           //       component: () => import('../pages/my/check.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'checklist',
//           //       name: 'checklist',
//           //       component: () => import('../pages/my/checklist.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'password',
//           //       name: 'password',
//           //       component: () => import('../pages/my/password.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'commandlist',
//           //       name: 'commandlist',
//           //       component: () => import('../pages/command/commandlist.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'remind',
//           //       name: 'remind',
//           //       component: () => import('../pages/remind/remind.vue') // 路由懒加载
//           //     },
//           //     {
//           //       path: 'commandqt',
//           //       name: 'commandqt',
//           //       component: () => import('../pages/command/commandqt.vue') // 路由懒加载
//           //     },
//           //
//           //   ]
//           // },
//           {
//             path: '/addsuccess',
//             name: 'addsuccess',
//             component: () => import('../pages/command/addsuccess.vue') // 路由懒加载
//           },
//           {
//             path: '/addcommand',
//             name: 'addcommand',
//             component: () => import('../pages/command/addcommand.vue') // 路由懒加载
//           },
//           {
//             path: '/searched',
//             name: 'searched',
//             component: () => import('../pages/product/searched.vue') // 路由懒加载
//           },
//           {
//             path: '/commanddetail',
//             name: 'commanddetail',
//             component: () => import('../pages/command/commanddetail.vue') // 路由懒加载
//           },
//           {
//             path: '/orderdetail',
//             name: 'orderdetail',
//             component: () => import('../pages/order/orderdetail.vue') // 路由懒加载
//           },
//            //产品
//   {
//     path: '/product',
//     name: 'product',
//     component: () => import('@/pages/product/product.vue'),
//     children: [
//       {
//         path: 'functionservice',
//         name: 'functionservice',
//         component: () => import('@/pages/index/functionservice.vue'),
//       }
//     ],
//   },
//   {
//     path: '/techservice',
//     name: 'techservice',
//     component: () => import('@/pages/index/techservice.vue'),
//   },
//   //平台
//   {
//     path: '/platform',
//     name: 'platform',
//     component: () => import('@/pages/index/cooperate.vue'),
//     children: [
//
//       {
//         path: 'cooperate',
//         name: 'cooperate',
//         component: () => import('@/pages/index/cooperate.vue'),
//       }
//     ]
//   },
//   //案例库
//   {
//     path: '/example',
//     name: 'example',
//     component: () => import('@/pages/index/example.vue'),
//   }
//         ]
//   },
//
//   {
//     path: '/test',
//     name: 'test',
//     component: () => import('@/pages/test.vue'),
//   },
//   {
//     path: '/forgotPwd',
//     hidden: true,
//     component: () => import('@/pages/forgotPwd.vue'),
//
//   },
//   {
//     path: '/modifyPwd',
//     name:'modifyPwd',
//     hidden: true,
//     component: () => import('@/pages/modifyPwd.vue')
//   },
//   {
//     path: '/login',
//     name: 'login',
//     component: () => import('@/pages/login.vue'),
//   },
//   {
//     path: '/register',
//     name: 'register',
//     component: () => import('@/pages/register.vue'),
//   },
//     {
//       path: '/wx',
//       name: 'wx',
//       component: () => import('@/pages/Login1.vue'),
//     },
//     {
//       path: '/home',
//       name: 'home',
//       component: () => import('@/pages/Home.vue'),
//     },
//   //个人中心
//
//
//
//
// ]

})
// router.beforeEach((to, from, next) => {
//   if (!TUICore.isLogin) {
//     next({ name: 'wx' });
//   } else {
//     next();
//   }
// });
export default router
