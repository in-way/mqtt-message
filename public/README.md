# 前端

1、mqtt配置
修改/config/mqtt.json
2、页面配置
在/page目录下新增页面，新增一个页面.json文件
3、页面访问
http://ip:port/devbus-control/?page=页面名称#/
3、page.json数据项说明

 [
   {
     "topic": "分组topic",
     "title": "组名",
     "child": [ //每组的元素
       {
         "label": "单个标签名",
         "type": "1",  //1输入框，2下拉
         "key": "消息key",
         "value": "消息值",
         "value_type": "number"  //value_type boolean，number,string
       },
       {
         "label": "单个标签名",
         "type": "2",
         "key": "消息key",
         "value": true,
         "value_type": "boolean",
         "option": [ //下拉选项
           {
             "label": "启动",
             "value": true
           },
           {
             "label": "停止",
             "value": false
           }
         ]
       }
     ]
   }

 ]